import React from "react";

type ItemGroupProps = {
  name?: string;
  onChange?: any;
};

const ItemGroup = ({ name, onChange }: ItemGroupProps) => (
  <div onChange={onChange} className="list-group-item">
    {name}
  </div>
);

export default ItemGroup;
