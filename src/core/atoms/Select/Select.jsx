import React from "react";
import { PropTypes } from "prop-types";

const Select = ({
  id,
  title,
  value,
  onChange,
  data,
  error,
  defaultName,
  defaultValue,
}) => {
  const option = data.map((item, idx) => (
    <option key={idx} value={item.value}>
      {item.name}
    </option>
  ));

  return (
    <div className="form-group mb-3">
      <label className="form-label" htmlFor={id}>
        {title}
      </label>
      <select className="form-select" value={value} onChange={onChange} id={id}>
        <option defaultValue disabled value={defaultValue}>
          {defaultName}
        </option>
        {option}
      </select>
    </div>
  );
};

export default Select;

Select.propTypes = {
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  value: PropTypes.string.isRequired,
  onChange: PropTypes.func.isRequired,
  defaultName: PropTypes.string.isRequired,
  defaultValue: PropTypes.string.isRequired,
  data: PropTypes.instanceOf(Array).isRequired,
};
