import React, { useEffect, useState } from "react";
import { PropTypes } from "prop-types";
import styles from "./styles.module.scss";
import { convertMinutestoSeconds, howManySeconds } from "./helper";

const Time = ({ getTime, time }) => {
  const [hours, setHours] = useState(Number(time.split(":")[0]));
  const [minutes, setMinutes] = useState(Number(time.split(":")[1]));

  useEffect(() => {
    getTime(howManySeconds(hours) + convertMinutestoSeconds(minutes));
  }, [hours, minutes, getTime]);

  return (
    <div className={`${styles.time}`}>
      <div className="me-2">
        <span>Hours</span>
        <input
          value={hours}
          onChange={(e) => setHours(e.target.value)}
          className="form-control"
          max={24}
          min={0}
          type="number"
        />
      </div>
      <div>
        <span>Minutes</span>
        <input
          onChange={(e) => setMinutes(e.target.value)}
          value={minutes}
          className="form-control"
          min={0}
          max={60}
          type="number"
        />
      </div>
    </div>
  );
};

export default Time;

Time.propTypes = {
  getTime: PropTypes.func.isRequired,
  time: PropTypes.string.isRequired,
};
