export const howManySeconds = (hours) => hours * 60 * 60;

export const convertMinutestoSeconds = (minutes) => Math.floor(minutes * 60);
