import React from "react";
import classNames from "classnames/bind";
import { PropTypes } from "prop-types";
import styles from "./styles.module.scss";

const Button = ({ onClick, type, className, typeColor, name, icon }) => {
  const cx = classNames.bind(styles);

  const colorButton = cx({
    btn: true,
    align: true,
    "btn-primary": typeColor === "primary",
    "btn-success": typeColor === "success",
    "btn-light": typeColor === "light",
    "btn-danger": typeColor === "danger",
    "btn-outline-danger": typeColor === "danger-outline",
  });

  return (
    <>
      <button
        type={type}
        onClick={onClick}
        className={`${colorButton} ${className}`}
      >
        {name}
        {icon && <span className="ms-1">{icon}</span>}
      </button>
    </>
  );
};

export default Button;

Button.defaultProps = {
  className: "",
  name: "",
  typeColor: "btn-primary",
  icon: null,
  onClick: null,
  type: "button",
};

Button.propTypes = {
  className: PropTypes.string,
  typeColor: PropTypes.string,
  name: PropTypes.string,
  onClick: PropTypes.func,
  icon: PropTypes.element,
  type: PropTypes.string,
};
