import React from "react";

interface TextFieldProps {
  id?: string;
  title?: string | undefined;
  value?: string;
  onChange?: any;
  error?: boolean;
  textError?: string;
  type?: string;
  placeholder?: string;
}
const TextField: React.FC<TextFieldProps> = ({
  id,
  title,
  value,
  onChange,
  error,
  textError,
  type,
  placeholder,
}) => {
  const isTextArea = type === "textarea";
  const column = 4;

  return (
    <>
      <div className="form-group mb-3">
        <label className="form-label" htmlFor={id}>
          {" "}
          {title}{" "}
        </label>

        {isTextArea ? (
          <textarea
            id={id}
            rows={column}
            value={value}
            onChange={onChange}
            placeholder={placeholder}
            className={`form-control ${error ? "is-invalid" : ""}`}
          />
        ) : (
          <input
            id={id}
            type="text"
            value={value}
            onChange={onChange}
            className={`form-control ${error ? "is-invalid" : ""}`}
            placeholder={placeholder}
          />
        )}

        {error && <div className="invalid-feedback">{textError}</div>}
      </div>
    </>
  );
};

export default TextField;
