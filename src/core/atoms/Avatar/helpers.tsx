export const shortString = (name: string) => {
    return name.slice(0, (name.length - name.length + 1)).toUpperCase();
}