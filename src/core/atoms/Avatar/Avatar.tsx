import React from "react";
import styles from "./styles.module.scss";
import { shortString } from "./helpers";

interface AvatartProps {
  link: string;
  profile: Profile;
}

interface Profile {
  name: string;
  lastName: string;
}

const Avatar: React.FC<AvatartProps> = ({ link, profile }) => {
  const name = shortString(profile.name);
  const lastName = shortString(profile.lastName);

  return (
    <div className={styles.avatar}>
      {profile ? (
        `${name}${lastName}`
      ) : link ? (
        <img src={link} alt="avatar" />
      ) : null}
    </div>
  );
};

export default Avatar;
