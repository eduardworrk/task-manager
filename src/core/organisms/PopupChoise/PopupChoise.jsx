import React from "react";
import { PropTypes } from "prop-types";
import Button from "../../atoms/Button/Button";
import styles from "./styles.module.scss";

export const PopupChoise = ({ title, closePopup, action }) => (
  <div className={styles.popupBackground}>
    <div className={styles.popup}>
      <h4 className="mb-4">{title}</h4>
      <Button
        name="Cancel"
        className="btn-lg"
        typeColor="light"
        onClick={closePopup}
      />

      <Button
        typeColor="danger"
        className="btn-lg ms-2"
        name="yes"
        onClick={action}
      />
    </div>
  </div>
);

export default PopupChoise;

PopupChoise.propTypes = {
  title: PropTypes.string.isRequired,
  closePopup: PropTypes.func.isRequired,
  action: PropTypes.func.isRequired,
};
