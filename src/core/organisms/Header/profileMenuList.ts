const profileMenuList = [
  {
    id: 1,
    name: "Профиль",
    link: "/profile",
  },
  {
    id: 2,
    name: "Выйти",
    link: "/login",
  },
];

export default profileMenuList;
