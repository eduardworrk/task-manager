import React, { useState, useEffect, useRef } from "react";
import classNames from "classnames/bind";
import { Link } from "react-router-dom";
import BeenhereIcon from "@material-ui/icons/Beenhere";
import styles from "./styles.module.scss";
import MainTimer from "../../../components/MainTimer/MainTimer";
import Menu from "../Menu/Menu";
import Avatar from "../../atoms/Avatar/Avatar";
import profileMenuList from "./profileMenuList";
import ProfileMenu from "./ProfileMenu";
import Search from "./Search";
import useOnClickOutside from "../../../hook/useOnclickOutside";

interface PropsHeader {
  currentTaskName: string;
  time: number;
  profile: Profile;
  getNameGlobalSearch: (name: string) => void;
  searchResult: any;
}

interface Profile {
  name: string;
  lastName: string;
}

const Header = ({
  currentTaskName,
  time,
  profile,
  getNameGlobalSearch,
  searchResult,
}: PropsHeader) => {
  const cx = classNames.bind(styles);

  const [open, setOpen] = useState<boolean>(false);

  const header = cx({
    header: true,
    start: !currentTaskName,
    between: true,
  });

  const handleOpenProfileMenu = () => {
    setOpen(!open);
  };

  const ref = useRef(null);

  const handleClickOutside = () => {
    setOpen(false);
  };

  useOnClickOutside(ref, handleClickOutside);

  return (
    <div className={header}>
      <div className={`${styles.headerMenu} main__header`}>
        <Link className={styles.logo} to="/projects">
          {" "}
          <BeenhereIcon />
          <span>Task Manager</span>
        </Link>

        <Menu />
        <Search
          result={searchResult}
          getNameGlobalSearch={getNameGlobalSearch}
        />
      </div>

      <div className="me-3 d-flex">
        <MainTimer title={currentTaskName} time={time} />
        <div className={styles.wrapMenu}>
          <div onClick={handleOpenProfileMenu}>
            <Avatar
              profile={profile}
              link="https://gitlab.com/uploads/-/system/user/avatar/1505853/avatar.png?width=90"
            />
          </div>
          {open && (
            <div ref={ref} className={styles.menu}>
              <ProfileMenu list={profileMenuList} />
            </div>
          )}
        </div>
      </div>
    </div>
  );
};

export default Header;
