import React, { useEffect, useState } from "react";
import styles from "./styles.module.scss";

interface SearchProps {
  getNameGlobalSearch: (name: string) => void;
  result: any;
}

const Search: React.FC<SearchProps> = ({ getNameGlobalSearch, result }) => {
  const [value, setValue] = useState<string>("");

  useEffect(() => {
    getNameGlobalSearch(value);
  }, [value, getNameGlobalSearch]);

  const resultTest = () => {
    if (!value) {
      return null;
    }

    return result.map((item: any) => {
      return <div className="list-group-item">{item.title}</div>;
    });
  };

  return (
    <div className={styles.globalSearch}>
      <input
        onChange={(e) => setValue(e.target.value)}
        placeholder="Search"
        className="form-control"
        type="text"
      />

      <div className={`${styles.search} list-group`}>{resultTest()}</div>
    </div>
  );
};

export default Search;
