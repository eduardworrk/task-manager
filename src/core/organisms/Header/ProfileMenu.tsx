import React from "react";
import { useHistory } from "react-router-dom";
import { setCookie } from "../../../services/helpers";

interface ProfileMenuProps {
  list: Item[];
}

interface Item {
  id: number;
  link: string;
  name: string;
}

const ProfileMenu: React.FunctionComponent<ProfileMenuProps> = ({ list }) => {
  const history = useHistory();

  const handleEvent = (item: string) => {
    if (item === "/logout") {
      setCookie("name", "");
    }

    history.push({
      pathname: item,
    });
  };

  const items = list.map((item: Item, id: number) => (
    <div
      key={id.toString()}
      onClick={() => handleEvent(item.link)}
      className="cursor-pointer list-group-item list-group-item-action"
    >
      {item.name}
    </div>
  ));

  return <div className="list-group">{items}</div>;
};

export default ProfileMenu;
