import React from "react";
import styles from "./styles.module.scss";

const Footer = () => (
  <footer className={styles.footer}>
    <div>
      Powered by{" "}
      <a target="_blank" href="https://gitlab.com/eduardworrk" rel="noreferrer">
        Eduard Valiyllin
      </a>{" "}
      2021
    </div>
  </footer>
);

export default Footer;
