import React from "react";
import { Link } from "react-router-dom";
import mainLanguage from "../../../services/lang";

const Menu = () => (
  <nav className="nav">
    <li className="nav-item">
      <Link className="nav-link text-white" to="/projects">
        {mainLanguage.menu.projects}
      </Link>
    </li>

    <li className="nav-item">
      <Link className="nav-link text-white" to="/statistics">
        {mainLanguage.menu.statistics}
      </Link>
    </li>
  </nav>
);

export default Menu;
