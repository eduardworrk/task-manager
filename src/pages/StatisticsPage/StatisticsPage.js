import React from "react";
import StatisticsContainer from "../../containers/StatisticsContainer/StatisticsContainer";

const StatisticsPage = () => <StatisticsContainer />;

export default StatisticsPage;
