import React from "react";
import TaskContainer from "../../containers/TaskContainer/TaskContainer";

const TaskPage = (option) => <TaskContainer option={option} />;

export default TaskPage;
