import React from "react";
import { Link } from "react-router-dom";
import en from "../../services/locale/en-en";

const Page404 = () => (
  <>
    <h3>{en.page404.title}</h3>
    <Link className="btn btn-link" to="/projects">
      {en.page404.link}
    </Link>
  </>
);

export default Page404;
