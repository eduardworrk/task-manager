import React from "react";
import { PropTypes } from "prop-types";
import ProjectContainer from "../../containers/ProjectContainer/ProjectContainer";

const ProjectPage = ({ currentTaskName, id, getTime }) => (
  <ProjectContainer
    paramsUrl={id}
    getTime={getTime}
    currentTaskName={currentTaskName}
  />
);

export default ProjectPage;

ProjectPage.defaultProps = {
  id: null,
  getTime: null,
  currentTaskName: null,
};

ProjectPage.propTypes = {
  id: PropTypes.object,
  getTime: PropTypes.func,
  currentTaskName: PropTypes.func,
};
