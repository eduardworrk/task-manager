import React from "react";
import LoginContainer from "../../containers/LoginContainer/LoginContainer";

interface LoginPageProps {
  getToken: (id: string) => void;
}

const LoginPage = ({ getToken }: LoginPageProps) => {
  return <LoginContainer getToken={getToken} />;
};

export default LoginPage;
