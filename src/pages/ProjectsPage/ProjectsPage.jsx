import React from "react";
import { PropTypes } from "prop-types";
import ProjectsContainers from "../../containers/ProjectsContainer/ProjectsContainers";
import ProjectPage from "../ProjectPage/ProjectPage";

const ProjectsPage = ({ projects, getTime }) => (
  <ProjectsContainers getTime={getTime} projects={projects} />
);

export default ProjectsPage;

ProjectPage.defaultProps = {
  getTime: null,
  currentTaskName: null,
  projects: [],
};

ProjectsPage.propTypes = {
  projects: PropTypes.instanceOf(Array),
  getTime: PropTypes.func,
};
