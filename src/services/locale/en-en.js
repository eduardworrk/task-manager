const en = {
  menu: {
    projects: "Projects",
    statistics: "Statistics",
  },
  projects: {
    name: "Name",
    enter: "Enter",
    newProject: "You have none projects. Please add the project.",
    createProjects: "Create projects",
    minSymbol: "Value empty,  value min 3 symbols",
    deleteProject: "Delete project",
    titleDeleteProject:
      "Are you ABSOLUTELY SURE you wish to delete this project?",
  },

  task: {
    name: "Name",
    description: "Description task",
    status: "Status",
    creation: "Creation at",
    time: "Time",
    priority: {
      easy: "Low",
      normal: "Medium",
      hard: "ASAP",
    },
    statuses: {
      select: "select",
      inProcess: "in process",
      inChecked: "on checked",
      done: "done",
    },
    table: {
      name: "Name",
      create: "Create at",
      priority: "Priority",
      status: "Status",
      action: "Action",
    },
    none: "Tasks none. You need create tasks",
  },

  statistics: {
    today: "Today",
    all: "All",
  },

  profile: {
    title: "My Profile Settings",
    name: "Name",
    lastName: "LastName",
    about:
      "I usually work from 9am-5pm PST. Feel free to assign me a task with a due date anytime. Also, I love dogs!",
  },

  page404: {
    title: "Page not found",
    link: "Back to main page",
  },

  button: {
    save: "Save",
    cancel: "Cancel",
    open: "Open",
    edit: "Edit",
  },

  errors: {
    fieldEmpty: "field empty",
  },
};

export default en;
