const ru = {
  menu: {
    projects: "Проекты",
    statistics: "Статистика",
  },
  projects: {
    name: "Название проекта",
    enter: "Введите",
    newProject: "У вас нет проектов. Добавьте пожалуйста проект.",
    createProjects: "Создать проект",
    minSymbol: "Пустое значение, мин 3 символа",
    deleteProject: "Удалить проект",
    titleDeleteProject:
      "Вы действительно хотите удалить проект? Все данные будут потеряны",
  },

  task: {
    name: "Название задачи",
    description: "Описание задачи",
    status: "Статус задачи",
    creation: "Создана",
    time: "Время выполнение задачи",
    priority: {
      easy: "Low",
      normal: "Medium",
      hard: "ASAP",
    },
    statuses: {
      select: "select",
      inProcess: "in process",
      inChecked: "on checked",
      done: "done",
    },
    table: {
      name: "Имя",
      create: "Создана",
      priority: "Приоритет",
      status: "Статус",
      action: "Действие",
    },
    none: "Пожалуйста добавьте задачу.",
  },

  statistics: {
    today: "Cегодня",
    all: "За все время",
  },

  profile: {
    title: "Настройки профиля",
    name: "Имя",
    lastName: "Фамилия",
    about:
      "Я работаю с 9.00-18.00 пишите и звоните мне в это время. В остальное время могу долго отвечать.",
  },

  page404: {
    title: "Такой страницы не существует",
    link: "Вернуться на главную",
  },

  button: {
    save: "Сохранить",
    cancel: "Отмена",
    open: "Открыть",
    edit: "Редактировать",
  },

  errors: {
    fieldEmpty: "Поле пустое, заполните пожалуйста",
  },
};

export default ru;
