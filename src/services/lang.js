import en from "./locale/en-en";
import ru from "./locale/ru-ru";

let language = window.navigator
  ? window.navigator.language ||
    window.navigator.systemLanguage ||
    window.navigator.userLanguage
  : "ru";
language = language.substr(0, 2).toLowerCase();

let mainLanguage = en;

if (language === "ru") {
  mainLanguage = ru;
}

export default mainLanguage;
