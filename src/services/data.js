const dataTask = {
  profile: {
    name: "",
    lastName: "",
    lang: "English",
    about: "",
  },
  projects: [
    {
      name: "Songkick",
      id: 122,
      tasks: [
        {
          id: "10",
          status: "in process",
          title: "Add new component radio button",
          description: "need to do and fill it data",
          time: 3400,
          start: false,
          priority: "ASAP",
          creation: "2021-09-18T20:23:28Z",
        },

        {
          id: "13",
          status: "done",
          title: "error log to the build",
          description: "find out what's wrong",
          time: 7400,
          start: false,
          priority: "Low",
          creation: "2021-07-19T16:23:28Z",
        },
        {
          id: "14",
          status: "done",
          title: "No bags",
          description: "find out what's wrong",
          time: 2600,
          start: false,
          priority: "Medium",
          creation: "2021-07-24T16:23:28Z",
        },
      ],
    },

    {
      name: "Asana",
      id: 1223,
      tasks: [
        {
          id: "23",
          status: "in process",
          title: "3ds secure for merchant warrior",
          description: "Description",
          time: 1000,
          start: false,
          priority: "Low",
          creation: "2021-06-12T16:23:28Z",
        },
      ],
    },
  ],
};

export default dataTask;
