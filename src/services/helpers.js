export const converDate = (date) =>
  new Date(date)
    .toISOString()
    .replace(/T.*/, "")
    .split("-")
    .reverse()
    .join("-");

export const convertTime = (sec) => {
  const date = new Date(1970, 0, 1);
  date.setSeconds(sec);
  return date.toTimeString().replace(/.*(\d{1}:\d{2}:\d{2}).*/, "$1");
};

export const convertTimeStatistics = (sec) => {
  const date = new Date(1970, 0, 1);
  date.setSeconds(sec);
  const stringReplaceData = date
    .toTimeString()
    .replace(/.*(\d{1}:\d{2}:\d{2}).*/, "$1");

  const replaceSymbol = /:/gi;

  const newstr = stringReplaceData.replace(replaceSymbol, ".");

  console.log(newstr);

  return Number(newstr);
};

function setCookie(cname, cvalue, exdays) {
  const d = new Date();
  d.setTime(d.getTime() + exdays * 24 * 60 * 60 * 1000);

  const expires = `expires=${d.toUTCString()}`;

  document.cookie = `${cname}=${cvalue};${expires};path=/;sameSite=lax;`;
}

function getCookie(cname, exact = true) {
  const name = `${cname}=`;
  const ca = document.cookie.split(";");
  for (let i = 0; i < ca.length; i++) {
    let c = ca[i];
    while (c.charAt(0) == " ") {
      c = c.substring(1);
    }

    if (exact) {
      if (c.indexOf(name) == 0) {
        return c.substring(name.length, c.length);
      }
    } else if (c.indexOf(cname) == 0) {
      return c.split("=")[1];
    }
  }
  return "";
}

export { setCookie, getCookie };

export const convertHMS = (value) => {
  const sec = parseInt(value, 10); // convert value to number if it's string
  let hours = Math.floor(sec / 3600); // get hours
  let minutes = Math.floor((sec - hours * 3600) / 60); // get minutes
  let seconds = sec - hours * 3600 - minutes * 60; //  get seconds
  // add 0 if value < 10; Example: 2 => 02
  if (hours < 10) {
    hours = `0${hours}`;
  }
  if (minutes < 10) {
    minutes = `0${minutes}`;
  }
  if (seconds < 10) {
    seconds = `0${seconds}`;
  }
  return Number(`${hours}.${minutes}`); // Return is HH : MM : SS
};
