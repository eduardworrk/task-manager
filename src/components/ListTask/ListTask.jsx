import React from "react";
import { PropTypes } from "prop-types";
import Task from "../Task/Task";
import mainLanguage from "../../services/lang";

const ListTask = ({
  listTasks,
  getTime,
  currentTaskName,
  handerGetIdCurrentTask,
  handerOpenPopup,
}) => {
  const currentProjectTaskList = () =>
    listTasks.map((item) => {
      if (item) {
        if (!item.tasks.length) {
          return <div className="p-3">{mainLanguage.task.none}</div>;
        }
        return item.tasks.map((element, idx) => (
          <>
            <Task
              idx={idx}
              id={element.id}
              key={element.id}
              getTime={getTime}
              time={element.time}
              title={element.title}
              status={element.status}
              creation={element.creation}
              priority={element.priority}
              currentTaskName={currentTaskName}
              handerOpenPopup={handerOpenPopup}
              handerGetIdCurrentTask={handerGetIdCurrentTask}
            />
          </>
        ));
      }
      return null;
    });

  return (
    <>
      <div className="card">
        <div className="card-body">
          <div className="table-responsive">
            <table className="table table-bordered">
              <thead>
                <tr>
                  <th scope="col">#</th>
                  <th scope="col">{mainLanguage.task.table.name}</th>
                  <th scope="col">{mainLanguage.task.table.create}</th>
                  <th scope="col">{mainLanguage.task.table.priority}</th>
                  <th scope="col">{mainLanguage.task.table.status}</th>
                  <th scope="col">{mainLanguage.task.table.action}</th>
                </tr>
              </thead>
              <tbody>{currentProjectTaskList()}</tbody>
            </table>
          </div>
        </div>
      </div>
    </>
  );
};

export default ListTask;

ListTask.propTypes = {
  getTime: PropTypes.func.isRequired,
  currentTaskName: PropTypes.func.isRequired,
  listTasks: PropTypes.instanceOf(Array).isRequired,
  handerOpenPopup: PropTypes.func.isRequired,
  handerGetIdCurrentTask: PropTypes.func.isRequired,
};
