import React, { useEffect, useState } from "react";
import TextField from "../../core/atoms/Input/TextField";
import styles from "./styles.module.scss";
import Button from "../../core/atoms/Button/Button";
import { getCookie, setCookie } from "../../services/helpers";

interface LoginProps {
  getToken: (id: string) => void;
  getLoginDate: (name: string, surname: string) => void;
}

const Login = ({ getToken, getLoginDate }: LoginProps) => {
  const [name, setName] = useState("");
  const [surname, setSurname] = useState("");

  const [errorName, setErrorName] = useState<boolean>(false);
  const [errorSurname, setErrorSurname] = useState<boolean>(false);

  useEffect(() => {
    if (getCookie("name")) {
      getToken(getCookie("name"));
    }
  }, [getToken]);

  const redirectForm = () => {
    if (!name) {
      setErrorName(true);
      return;
    }

    setErrorName(false);

    if (!surname) {
      setErrorSurname(true);
      return;
    }

    setErrorName(false);

    setCookie("name", name, 30);

    getLoginDate(name, surname);
    getToken(name);
  };

  return (
    <div className={styles.wrapLogin}>
      <div className={styles.login}>
        <div className="card">
          <div className="card-body">
            <TextField
              value={name}
              id="name"
              textError="error"
              error={errorName}
              onChange={(ev: React.ChangeEvent<HTMLInputElement>): void =>
                setName(ev.target.value)
              }
              title="First name"
            />

            <TextField
              onChange={(ev: React.ChangeEvent<HTMLInputElement>): void =>
                setSurname(ev.target.value)
              }
              value={surname}
              id="surname"
              error={errorSurname}
              textError="error"
              title="Surname"
            />

            <div className="d-grid gap-2">
              <Button name="Login" typeColor="primary" onClick={redirectForm} />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Login;
