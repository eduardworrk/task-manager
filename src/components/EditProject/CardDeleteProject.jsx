import React, { useState } from "react";
import { PropTypes } from "prop-types";
import TextField from "../../core/atoms/Input/TextField";
import Button from "../../core/atoms/Button/Button";
import mainLanguage from "../../services/lang";

const CardDeleteProject = ({ nameProject, deleteProject, cancelShowError }) => {
  const [checkNameProject, setCheckNameProject] = useState("");
  const [error, setError] = useState(false);

  const checkProject = () => {
    if (checkNameProject === nameProject) {
      deleteProject();
    }
    setError(true);
  };

  const cancel = () => cancelShowError(false);

  return (
    <div className="card text-dark bg-light">
      <div className="card-body">
        <TextField
          error={error}
          id="deteteProject"
          value={checkNameProject}
          textError="Enter name project"
          placeholder={`${mainLanguage.projects.enter} ${nameProject}`}
          onChange={(e) => setCheckNameProject(e.target.value)}
          title={mainLanguage.projects.titleDeleteProject}
        />

        <Button onClick={cancel} name={mainLanguage.button.cancel} />
        <Button
          className="ms-1"
          typeColor="danger"
          onClick={checkProject}
          name={mainLanguage.projects.deleteProject}
        />
      </div>
    </div>
  );
};

export default CardDeleteProject;

CardDeleteProject.propTypes = {
  nameProject: PropTypes.string.isRequired,
  deleteProject: PropTypes.func.isRequired,
  cancelShowError: PropTypes.func.isRequired,
};
