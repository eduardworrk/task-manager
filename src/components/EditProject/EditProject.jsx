import React, { useState } from "react";
import { PropTypes } from "prop-types";
import mainLanguage from "../../services/lang";
import Button from "../../core/atoms/Button/Button";
import CardDeleteProject from "./CardDeleteProject";
import TextField from "../../core/atoms/Input/TextField";

const EditProject = ({ deleteProject, renameProject, back, name }) => {
  const [nameProject, setNameProject] = useState(
    name.join().replace(/[\s.,%]/g, "")
  );
  const [showTableDelete, setShowTableDelete] = useState(false);

  const showTableError = () => {
    setShowTableDelete(true);
  };

  const cancelShowError = (error) => setShowTableDelete(error);

  const saveForm = () => {
    renameProject(nameProject);
  };

  return (
    <div className="col-lg-6">
      <div className="card">
        <div className="card-body">
          <TextField
            id="projectName"
            value={nameProject}
            textError="requred"
            error={!nameProject.length}
            title={mainLanguage.projects.name}
            onChange={(e) => setNameProject(e.target.value)}
          />

          {!showTableDelete && (
            <div className="form-group mb-3">
              <Button
                typeColor="danger-outline"
                onClick={showTableError}
                name={mainLanguage.projects.deleteProject}
              />
            </div>
          )}

          {showTableDelete && (
            <CardDeleteProject
              nameProject={nameProject}
              deleteProject={deleteProject}
              cancelShowError={cancelShowError}
            />
          )}

          <hr />
          <div className="form-group">
            <Button
              name={mainLanguage.button.cancel}
              className="me-2"
              type="light"
              onClick={back}
            />

            <button
              type="button"
              onClick={saveForm}
              className="btn btn-success"
            >
              {mainLanguage.button.save}
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditProject;

EditProject.propTypes = {
  name: PropTypes.object.isRequired,
  renameProject: PropTypes.func.isRequired,
  deleteProject: PropTypes.func.isRequired,
  back: PropTypes.func.isRequired,
};
