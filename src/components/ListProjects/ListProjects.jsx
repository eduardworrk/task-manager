import React from "react";
import { useDispatch } from "react-redux";
import { Link } from "react-router-dom";
import { PropTypes } from "prop-types";
import { currentProject } from "../../module/action/action";
import en from "../../services/locale/en-en";

const ListProjects = ({ projects }) => {
  const dispatch = useDispatch();

  const getIdElement = (id) => {
    dispatch(currentProject(id));
  };

  return (
    <nav>
      <div>
        <div className="row">
          {projects.length ? (
            projects.map((i) => (
              <div key={i.id} className="col-lg-3">
                <div className="card mb-3">
                  <div className="card-body">
                    <h5 className="card-title">{i.name}</h5>
                  </div>

                  <div className="card-footer">
                    <Link
                      className="btn btn-outline-primary me-1"
                      onClick={() => getIdElement(i.id)}
                      key={i.id}
                      to={{ pathname: `/projects/${i.id}` }}
                    >
                      {en.button.open}
                    </Link>

                    <Link
                      to={{ pathname: `/projects/${i.id}/edit` }}
                      className="btn btn-outline-secondary"
                    >
                      Edit
                    </Link>
                  </div>
                </div>
              </div>
            ))
          ) : (
            <div className="col-lg-12">{en.projects.newProject}</div>
          )}
        </div>
      </div>
    </nav>
  );
};

export default ListProjects;

ListProjects.propTypes = {
  projects: PropTypes.instanceOf(Array).isRequired,
};
