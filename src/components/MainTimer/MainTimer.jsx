import React, { useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import StopIcon from "@material-ui/icons/Stop";
import PauseIcon from "@material-ui/icons/Pause";
import { useHistory } from "react-router-dom";
import { PropTypes } from "prop-types";
import {
  saveTimerTask,
  startTask,
  pauseTask,
} from "../../module/action/action";
import styles from "./styles.module.scss";
import { convertTime } from "../../services/helpers";

const MainTimer = ({ time, title }) => {
  const playCurrentTask = useSelector((state) => state.main.startTask);
  const currentTaskId = useSelector((state) => state.main.currentTask);
  const currentProjectId = useSelector((state) => state.main.currentProject);

  const dispatch = useDispatch();
  const history = useHistory();

  const timerStart = useRef(0);
  const [timer, setTimer] = useState(0);

  const startHander = () => {
    if (timerStart.current) {
      return;
    }
    timerStart.current = setInterval(() => setTimer((c) => c + 1), 1000);
    dispatch(startTask(true, currentTaskId));
  };

  const pauseHandler = () => {
    clearInterval(timerStart.current);
    timerStart.current = 0;
    dispatch(pauseTask(false));
    // dispatch(startTask(false, currentTaskId));
  };

  useEffect(() => () => clearInterval(timerStart.current), []);

  useEffect(() => {
    if (playCurrentTask) {
      startHander();
    }
  }, [playCurrentTask]);

  const stop = () => {
    clearInterval(timerStart.current);
    timerStart.current = 0;
    dispatch(saveTimerTask(time + timer, currentTaskId));
    setTimer(0);
    history.push({
      pathname: `/projects/${currentProjectId}/task/${currentTaskId}`,
      taskId: currentTaskId,
    });
  };

  return (
    <>
      {currentTaskId ? (
        <div className={styles.current__task}>
          <span className={styles.task}>
            {title} ({convertTime(timer)})
          </span>

          <div className="btn-group" role="group" aria-label="Basic example">
            {!playCurrentTask && (
              <button
                type="button"
                onClick={startHander}
                className="btn btn-primary"
              >
                <PlayArrowIcon />
              </button>
            )}

            {playCurrentTask && (
              <button
                type="button"
                onClick={pauseHandler}
                className="btn btn-primary"
              >
                <PauseIcon />
              </button>
            )}

            <button type="button" onClick={stop} className="btn btn-primary">
              <StopIcon />
            </button>
          </div>
        </div>
      ) : null}
    </>
  );
};

export default MainTimer;

MainTimer.propTypes = {
  time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
};
