import React, { useEffect, useState, FormEvent } from "react";
import { useHistory } from "react-router-dom";
import TextField from "../../core/atoms/Input/TextField";
import Button from "../../core/atoms/Button/Button";
import Select from "../../core/atoms/Select/Select";
import option from "./data";
import mainLanguage from "../../services/lang";

interface ProfileFormProps {
  name: string;
  lastName: string;
  about: string;
  language: string;
  editForm: (
    name: string,
    lastName: string,
    language: string,
    about: string
  ) => void;
  back: () => void;
}

const ProfileForm: React.FunctionComponent<ProfileFormProps> = ({
  name,
  lastName,
  editForm,
  language,
  about,
  back,
}) => {
  const [names, setName] = useState<string>(name);
  const [lastNames, setLastName] = useState<string>(lastName);
  const [aboutMe, setAboutMe] = useState<string>(about);
  const [languageChoise, setLanguageChoise] = useState<string>(language);

  const history = useHistory();

  const saveEditForm = (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();

    editForm(names, lastNames, languageChoise, aboutMe);

    history.push({
      pathname: "/projects",
    });
  };

  return (
    <div className="card">
      <h5 className="card-header p-3">{mainLanguage.profile.title}</h5>
      <div className="card-body">
        <form onSubmit={saveEditForm}>
          <TextField
            id="name"
            title={mainLanguage.profile.name}
            value={names}
            onChange={(ev: React.ChangeEvent<HTMLInputElement>): void =>
              setName(ev.target.value)
            }
          />

          <TextField
            value={lastNames}
            title={mainLanguage.profile.lastName}
            id="lastName"
            onChange={(ev: React.ChangeEvent<HTMLInputElement>): void =>
              setLastName(ev.target.value)
            }
          />

          <TextField
            placeholder={mainLanguage.profile.about}
            value={aboutMe}
            onChange={(ev: React.ChangeEvent<HTMLInputElement>): void =>
              setAboutMe(ev.target.value)
            }
            type="textarea"
          />

          {/*<Select*/}
          {/*  defaultName="English"*/}
          {/*  defaultValue="English"*/}
          {/*  title="Language"*/}
          {/*  id="lang"*/}
          {/*  onChange={(ev: React.ChangeEvent<HTMLInputElement>): void =>*/}
          {/*    setLanguageChoise(ev.target.value)*/}
          {/*  }*/}
          {/*  data={option}*/}
          {/*  value={language}*/}
          {/*/>*/}

          <Button
            onClick={back}
            name={mainLanguage.button.cancel}
            typeColor="light"
          />

          <Button
            className="ms-2"
            type="submit"
            name={mainLanguage.button.save}
            typeColor="success"
          />
        </form>
      </div>
    </div>
  );
};

export default ProfileForm;
