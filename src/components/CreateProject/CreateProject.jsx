import React, { useState } from "react";
import { useSelector } from "react-redux";
import { PropTypes } from "prop-types";
import Button from "../../core/atoms/Button/Button";
import mainLanguage from "../../services/lang";
import TextField from "../../core/atoms/Input/TextField";

const CreateProject = ({
  handerGetDataProject,
  getDataCurrentIdProject,
  handlerCloseProject,
}) => {
  const popupProject = useSelector(
    (state) => state.main.openCreateProjectPopup
  );

  const [nameProject, setNameProject] = useState("");
  const [error, setError] = useState(false);

  const saveProject = () => {
    const idProject = Math.floor(Math.random() * 100000) + 1;

    if (!nameProject) {
      setError(true);
      return;
    }

    handerGetDataProject({
      name: nameProject,
      id: idProject,
      tasks: [],
    });

    getDataCurrentIdProject(idProject);
    handlerCloseProject();
  };

  return popupProject ? (
    <div className="col-lg-6">
      <div className="card">
        <div className="card-body">
          <TextField
            error={error}
            value={nameProject}
            id="createProject"
            textError={mainLanguage.projects.minSymbol}
            title={mainLanguage.projects.createProjects}
            onChange={(e) => setNameProject(e.target.value)}
          />

          <div className="form-group">
            <Button
              onClick={handlerCloseProject}
              name={mainLanguage.button.cancel}
              typeColor="light"
            />
            <Button
              onClick={saveProject}
              name={mainLanguage.button.save}
              typeColor="success"
              className="ms-2"
            />
          </div>
        </div>
      </div>
    </div>
  ) : null;
};

export default CreateProject;

CreateProject.propTypes = {
  handerGetDataProject: PropTypes.func.isRequired,
  getDataCurrentIdProject: PropTypes.func.isRequired,
  handlerCloseProject: PropTypes.func.isRequired,
};
