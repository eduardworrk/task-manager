import React, { useState } from "react";
import { PropTypes } from "prop-types";
import { useHistory } from "react-router-dom";
import { useDispatch, useSelector } from "react-redux";
import { saveTask } from "../../module/action/action";
import Time from "../../core/atoms/Time/Time";
import mainLanguage from "../../services/lang";
import { converDate, convertTime } from "../../services/helpers";
import TextField from "../../core/atoms/Input/TextField";
import Button from "../../core/atoms/Button/Button";
import Select from "../../core/atoms/Select/Select";
import option from "./data";

const EditTask = ({
  creation,
  description,
  id,
  status,
  time,
  title,
  currentProjectId,
  back,
}) => {
  const [titleTask, setTitleTask] = useState(title);
  const [descriptionTask, setDescriptionTask] = useState(description);
  const [statusTask, setStatusTask] = useState(status);
  const timeCurrentTask = useSelector((state) => state.main.currentTaskTime);
  const [timeTask, setTimeTask] = useState(convertTime(time));

  const [formatDate, setFormatDate] = useState(0);

  const history = useHistory();
  const dispatch = useDispatch();

  const saveForm = (e) => {
    e.preventDefault();
    dispatch(
      saveTask(
        {
          id,
          time: formatDate,
          title: titleTask,
          status: statusTask,
          description: descriptionTask,
        },
        0
      )
    );

    history.push(`/projects/${currentProjectId}`);
  };

  const getTime = (times) => {
    setFormatDate(times);
  };

  return (
    <div className="row">
      <div className="col-lg-8">
        <div className="card">
          <form onSubmit={saveForm} className="card-body">
            <TextField
              id="title"
              value={titleTask}
              textError="Required"
              error={!titleTask.length}
              title={mainLanguage.task.name}
              onChange={(e) => setTitleTask(e.target.value)}
            />

            <TextField
              type="textarea"
              id="description"
              textError="Required"
              value={descriptionTask}
              error={!descriptionTask.length}
              title={mainLanguage.task.description}
              onChange={(e) => setDescriptionTask(e.target.value)}
            />

            <Select
              id="status"
              data={option}
              value={statusTask}
              defaultName="select"
              title={mainLanguage.task.status}
              onChange={(e) => setStatusTask(e.target.value)}
              defaultValue={mainLanguage.task.statuses.select}
            />

            <Button
              onClick={back}
              typeColor="light"
              name={mainLanguage.button.cancel}
            />

            <button type="submit" className="btn-success btn ms-2">
              {mainLanguage.button.save}
            </button>
          </form>
        </div>
      </div>

      <div className="col-lg-4">
        <div className="card">
          <div className="card-body">
            <TextField
              value={converDate(creation)}
              id="taskCreation"
              title={mainLanguage.task.creation}
            />

            <div className="mb-3">
              <div className="mb-2">
                <span>{mainLanguage.task.time}</span>
              </div>

              <Time
                time={timeTask}
                getTime={getTime}
                currentTime={timeCurrentTask}
              />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default EditTask;

EditTask.propTypes = {
  id: PropTypes.string.isRequired,
  back: PropTypes.func.isRequired,
  time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  status: PropTypes.string.isRequired,
  creation: PropTypes.string.isRequired,
  description: PropTypes.string.isRequired,
  currentProjectId: PropTypes.number.isRequired,
};
