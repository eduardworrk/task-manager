import mainLanguage from "../../services/lang";

export const option = [
  {
    name: "in process",
    value: mainLanguage.task.statuses.inProcess,
  },

  {
    name: "done",
    value: mainLanguage.task.statuses.done,
  },

  {
    name: "in checked",
    value: mainLanguage.task.statuses.inChecked,
  },
];

export default option;
