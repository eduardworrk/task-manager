import React, { useState } from "react";
import { useDispatch } from "react-redux";
import classNames from "classnames/bind";
import { addElement } from "../../module/action/action";
import "react-toastify/dist/ReactToastify.css";
import { useHistory } from "react-router-dom";
import styles from "./style.module.scss";
import { PropTypes } from "prop-types";
import mainLanguage from "../../services/lang";
import TextField from "../../core/atoms/Input/TextField";
import Button from "../../core/atoms/Button/Button";
import Select from "../../core/atoms/Select/Select";
import option from "./data";

const NewTask = ({ currentProjectId, back }) => {
  const [nameTask, setNameTask] = useState("");
  const [description, setDescription] = useState("");
  const [statusTask, setStatusTask] = useState("");
  const [priority, setPriority] = useState("");

  const date = new Date();
  const dataTime = date.toISOString();

  const [errors, setErrors] = useState({
    name: false,
    description: false,
  });

  const history = useHistory();
  const dispatch = useDispatch();

  const onSubmit = (e) => {
    e.preventDefault();
    const array = {};
    const id = Math.floor(Math.random() * 10000);

    if (!nameTask) {
      setErrors((element) => ({ ...element, name: true }));
      return;
    }
    setErrors((element) => ({ ...element, name: false }));

    if (!description) {
      setErrors((element) => ({ ...element, description: true }));
      return;
    }
    setErrors((element) => ({ ...element, description: false }));

    array.id = id;
    array.time = 0;
    array.start = false;
    array.title = nameTask;
    array.description = description;
    array.status = statusTask;
    array.priority = priority;
    array.creation = dataTime;

    dispatch(addElement(array));
    e.target.reset();
    history.push(`/projects/${currentProjectId}`);
  };

  const cx = classNames.bind(styles);

  const colorButton = cx({
    card: true,
  });

  return (
    <div className="row">
      <div className="col-lg-8">
        <div className={colorButton}>
          <form onSubmit={onSubmit} className="card-body">
            <TextField
              id="title"
              value={nameTask}
              error={errors.name}
              textError={mainLanguage.errors.fieldEmpty}
              title={mainLanguage.task.name}
              onChange={(e) => setNameTask(e.target.value)}
            />

            <TextField
              id="title"
              type="textarea"
              value={description}
              error={errors.description}
              textError={mainLanguage.errors.fieldEmpty}
              title={mainLanguage.task.description}
              onChange={(e) => setDescription(e.target.value)}
            />

            <div className={styles.listPriority}>
              <div className="mb-3">Приоритет</div>
              <div className={styles.listWrapPriority}>
                <div className={styles.customRadio}>
                  <input
                    className={styles.priorityRadio}
                    name="priority"
                    id="Low"
                    onChange={(e) => setPriority(e.target.id)}
                    type="radio"
                  />
                  <label htmlFor="Low">{mainLanguage.task.priority.easy}</label>
                </div>

                <div className={styles.customRadio}>
                  <input
                    className={styles.priorityRadio}
                    name="priority"
                    id="Medium"
                    type="radio"
                    onChange={(e) => setPriority(e.target.id)}
                  />
                  <label htmlFor="Medium">
                    {mainLanguage.task.priority.normal}
                  </label>
                </div>

                <div className={styles.customRadio}>
                  <input
                    className={styles.priorityRadio}
                    name="priority"
                    id="ASAP"
                    type="radio"
                    onChange={(e) => setPriority(e.target.id)}
                  />
                  <label htmlFor="ASAP">
                    {mainLanguage.task.priority.hard}
                  </label>
                </div>
              </div>
            </div>

            <Select
              data={option}
              id="status"
              title="Status"
              defaultValue="select"
              defaultName={mainLanguage.task.statuses.select}
              value={statusTask}
              onChange={(e) => setStatusTask(e.target.value)}
            />

            <Button
              name={mainLanguage.button.cancel}
              typeColor="light"
              onClick={back}
            />

            <Button
              type="submit"
              className="ms-2"
              typeColor="success"
              name={mainLanguage.button.save}
            />
          </form>
        </div>
      </div>
    </div>
  );
};

export default NewTask;

NewTask.propTypes = {
  back: PropTypes.func.isRequired,
  currentProjectId: PropTypes.number.isRequired,
};
