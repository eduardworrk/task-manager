import React, { useEffect, useState } from "react";
import {
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
  ResponsiveContainer,
} from "recharts";
import { useSelector } from "react-redux";
import moment from "moment";
import { convertHMS } from "../../services/helpers";

const BarStatistics = () => {
  const projects = useSelector((state) => state.main.projects);
  const [joinTasks, setJoinTasks] = useState([]);

  useEffect(() => {
    const array = [];
    projects.forEach((item) => {
      array.push(item.tasks);
    });
    setJoinTasks(array);
  }, [projects]);

  const allTasks = joinTasks.flat().map((item) => ({
    format: item.creation,
    day: moment(item.creation).format("DD.MM.YYYY"),
    time: convertHMS(item.time),
  }));

  allTasks.sort((a, b) => new Date(a.format) - new Date(b.format));

  const result = allTasks.reduce(
    (acc, cur) => {
      const sameDay = acc.map[cur.day];
      if (typeof sameDay !== "undefined") {
        sameDay.time += cur.time;
      } else {
        acc.map[cur.day] = { ...cur };
        acc.res.push(acc.map[cur.day]);
      }
      return acc;
    },
    { map: {}, res: [] }
  ).res;

  return (
    <div className="row">
      <div className="col-lg-12">
        <div className="card">
          <div className="card-body">
            <div
              className="btn-group"
              role="group"
              aria-label="Basic outlined example"
            >
              <button type="button" className="btn btn-outline-primary">
                Сегодня
              </button>
              <button type="button" className="btn btn-outline-primary">
                Все время
              </button>
            </div>

            <div className="mt-5" style={{ width: "100%", height: 350 }}>
              <ResponsiveContainer width="100%" height="100%">
                <BarChart
                  width={800}
                  height={300}
                  data={result}
                  margin={{
                    top: 5,
                    right: 30,
                    left: 20,
                    bottom: 5,
                  }}
                >
                  <CartesianGrid strokeDasharray="3 3" />
                  <XAxis dataKey="day" />
                  <YAxis />
                  <Tooltip />
                  <Legend />
                  <Bar dataKey="time" fill="#276AEB" />
                </BarChart>
              </ResponsiveContainer>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default BarStatistics;
