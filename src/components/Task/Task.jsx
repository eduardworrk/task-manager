import React, { useState } from "react";
import classNames from "classnames/bind";
import PlayArrowIcon from "@material-ui/icons/PlayArrow";
import EditIcon from "@material-ui/icons/Edit";
import DeleteIcon from "@material-ui/icons/Delete";
import { useDispatch } from "react-redux";
import { useHistory, useLocation } from "react-router-dom";
import { PropTypes } from "prop-types";
import WhatshotIcon from "@material-ui/icons/Whatshot";
import { startTask } from "../../module/action/action";

import styles from "./styles.module.scss";
import { converDate } from "../../services/helpers";
import mainLanguage from "../../services/lang";

const Task = ({
  id,
  idx,
  time,
  title,
  status,
  priority,
  creation,
  getTime,
  currentTaskName,
  handerGetIdCurrentTask,
  handerOpenPopup,
}) => {
  const [visible, setVisible] = useState(false);
  const dispatch = useDispatch();

  const history = useHistory();
  const location = useLocation();

  const openDetails = () => {
    setVisible(visible === false);

    history.push({
      pathname: `${location.pathname}/task/${id}`,
      taskId: id,
    });
  };

  const taskData = converDate(creation);

  const startTime = () => {
    dispatch(startTask(true, id));
    getTime(time);
    currentTaskName(title);
  };

  const deleteTask = () => {
    handerGetIdCurrentTask(id);
    handerOpenPopup(true);
  };

  const cx = classNames.bind(styles);

  const colorButton = cx({
    task: true,
    completed: status === "done",
    process: status === "in process",
    white: visible,
  });

  const badgeColorPriority = () => {
    if (priority === mainLanguage.task.priority.hard) {
      return (
        <span className="badge bg-danger">
          {priority} <WhatshotIcon />
        </span>
      );
    }

    if (priority === mainLanguage.task.priority.normal) {
      return <span className="badge bg-warning text-dark">{priority} </span>;
    }

    if (priority === mainLanguage.task.priority.easy) {
      return <span className="badge bg-success">{priority} </span>;
    }
    return null;
  };

  return (
    <tr key={id}>
      <th scope="row">{idx}</th>
      <th scope="row">{title}</th>
      <td>{taskData}</td>
      <td>{badgeColorPriority()}</td>
      <td className={colorButton}>{status}</td>
      <td>
        <div className="btn-group">
          <button
            type="button"
            className="btn-sm btn btn-primary"
            onClick={startTime}
          >
            <PlayArrowIcon />
          </button>

          <button
            type="button"
            onClick={openDetails}
            className="btn btn-primary btn-sm"
          >
            <EditIcon />
          </button>
          <button type="button" className="btn btn-danger" onClick={deleteTask}>
            <DeleteIcon />
          </button>
        </div>
      </td>
    </tr>
  );
};

export default Task;

Task.propTypes = {
  id: PropTypes.string.isRequired,
  idx: PropTypes.number.isRequired,
  time: PropTypes.number.isRequired,
  title: PropTypes.string.isRequired,
  getTime: PropTypes.func.isRequired,
  status: PropTypes.string.isRequired,
  creation: PropTypes.string.isRequired,
  priority: PropTypes.string.isRequired,
  handerOpenPopup: PropTypes.func.isRequired,
  currentTaskName: PropTypes.func.isRequired,
  handerGetIdCurrentTask: PropTypes.func.isRequired,
};
