import React from "react";
import BarStatistics from "../../components/BarStatistic/BarStatistic";

const StatisticsContainer = () => <BarStatistics />;

export default StatisticsContainer;
