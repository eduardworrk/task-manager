import React from "react";
import { useDispatch } from "react-redux";
import Login from "../../components/Login/Login";
import { saveProfile } from "../../module/action/action";

interface LoginContainerProps {
  getToken: (id: string) => void;
}

const LoginContainer = ({ getToken }: LoginContainerProps) => {
  const dispatch = useDispatch();
  const getLoginDate = (value: string, value2: string) => {
    dispatch(saveProfile({ name: value, lastName: value2 }));
  };

  return <Login getToken={getToken} getLoginDate={getLoginDate} />;
};

export default LoginContainer;
