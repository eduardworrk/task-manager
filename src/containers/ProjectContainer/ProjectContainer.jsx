import * as React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { PropTypes } from "prop-types";
import AddIcon from "@material-ui/icons/Add";
import {
  currentProject,
  deleteProject,
  deleteTask,
  renameProject,
} from "../../module/action/action";
// eslint-disable-next-line import/no-cycle
import ListTask from "../../components/ListTask/ListTask";
import styles from "./style.module.scss";
import EditProject from "../../components/EditProject/EditProject";
import Button from "../../core/atoms/Button/Button";
import PopupChoise from "../../core/organisms/PopupChoise/PopupChoise";

const ProjectContainer = ({ currentTaskName, paramsUrl, getTime }) => {
  const currentProjectId = useSelector((state) => state.main.currentProject);
  const projects = useSelector((state) => state.main.projects);
  const [editPage, setEditPage] = useState(false);
  const [deletePopup, setDeletePopup] = useState(false);
  const [getIdCurrentTask, setGetIdCurrentTask] = useState(null);

  const { id } = useParams();
  const history = useHistory();
  const dispatch = useDispatch();

  const currentTasksProject = projects.filter((item) => {
    if (item.id === currentProjectId) {
      return item;
    }

    return null;
  });

  const createTask = () => {
    history.push(`/projects/${id}/create-task`);
  };
  const handerGetIdCurrentTask = (idTask) => {
    setGetIdCurrentTask(idTask);
  };
  // get current id task

  const deleteTaskComponent = () => {
    dispatch(deleteTask(getIdCurrentTask));
    setDeletePopup(false);
  };

  const handerOpenPopup = (value) => {
    setDeletePopup(value);
  };
  const handlerClosePopup = () => {
    setDeletePopup(false);
  };

  useEffect(() => {
    dispatch(currentProject(Number(id)));
  });

  useEffect(() => {
    if (paramsUrl) {
      if (paramsUrl.match.params.slug === "edit") {
        setEditPage(true);
      }
    }
  }, [paramsUrl]);

  const editNameProject = (name) => {
    dispatch(renameProject(name));
    history.push({
      pathname: "/projects",
    });
  };

  const currentDeleteProject = () => {
    dispatch(deleteProject(currentProjectId));
    history.push({
      pathname: "/projects",
    });
  };

  const currentProjectNames = projects.map((item) => {
    if (item.id === currentProjectId) {
      return item.name;
    }
    return null;
  });

  const back = () => history.goBack();

  return (
    <>
      {editPage ? (
        <EditProject
          back={back}
          name={currentProjectNames}
          renameProject={editNameProject}
          deleteProject={currentDeleteProject}
        />
      ) : (
        <div>
          <div className="row">
            <div className="col-lg-12">
              <div className={styles.header}>
                <h3>{currentProjectNames}</h3>

                <Button
                  icon={<AddIcon />}
                  typeColor="success"
                  name="Create task"
                  onClick={createTask}
                />
              </div>
            </div>
          </div>

          <div className="row">
            <div className="col-lg-12">
              <ListTask
                getTime={getTime}
                listTasks={currentTasksProject}
                currentTaskName={currentTaskName}
                handerOpenPopup={handerOpenPopup}
                handerGetIdCurrentTask={handerGetIdCurrentTask}
              />
            </div>
          </div>
        </div>
      )}

      {deletePopup && (
        <PopupChoise
          closePopup={handlerClosePopup}
          action={deleteTaskComponent}
          title="Do you want delete task?"
        />
      )}
    </>
  );
};

export default ProjectContainer;

ProjectContainer.propTypes = {
  currentTaskName: PropTypes.func.isRequired,
  getTime: PropTypes.func.isRequired,
  paramsUrl: PropTypes.object,
};
