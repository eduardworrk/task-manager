import React from "react";
import { useLocation } from "react-router";
import { useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import NewTask from "../../components/NewTask/NewTask";
import EditTask from "../../components/EditTask/EditTask";

const TaskContainer = () => {
  const location = useLocation();

  const currentProjectId = useSelector((state) => state.main.currentProject);
  const projects = useSelector((state) => state.main.projects);
  const currentProjectData = projects.find(
    (item) => item.id === currentProjectId
  );
  const currentTask = currentProjectData.tasks.find(
    (item) => item.id === location.taskId
  );
  const isEditTask = useSelector((state) => state.main.editTask);

  const history = useHistory();
  const back = () => history.goBack();

  return (
    <>
      {location.taskId || isEditTask ? (
        <EditTask
          back={back}
          id={currentTask.id}
          time={currentTask.time}
          title={currentTask.title}
          start={currentTask.start}
          status={currentTask.status}
          creation={currentTask.creation}
          currentProjectId={currentProjectId}
          description={currentTask.description}
        />
      ) : (
        <NewTask back={back} currentProjectId={currentProjectId} />
      )}
    </>
  );
};
export default TaskContainer;
