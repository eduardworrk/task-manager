import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { PropTypes } from "prop-types";
import AddIcon from "@material-ui/icons/Add";
import {
  addProject,
  currentProject,
  openPopupProject,
} from "../../module/action/action";
import ListProjects from "../../components/ListProjects/ListProjects";
import CreateProject from "../../components/CreateProject/CreateProject";
import Button from "../../core/atoms/Button/Button";

const ProjectsContainers = ({ getTime }) => {
  const projectss = useSelector((state) => state.main.projects);
  const isOpenPopupProject = useSelector(
    (state) => state.main.openCreateProjectPopup
  );

  const dispatch = useDispatch();

  const openCreateProject = () => {
    dispatch(openPopupProject(true));
  };

  const handerGetDataProject = (element) => {
    dispatch(addProject(element));
  };

  const getDataCurrentIdProject = (id) => {
    dispatch(currentProject(id));
  };

  const handlerCloseProject = () => dispatch(openPopupProject(false));

  return (
    <div>
      {!isOpenPopupProject ? (
        <div>
          <Button
            className="btn btn-primary mb-4"
            name="Create project"
            typeColor="success"
            icon={<AddIcon />}
            onClick={openCreateProject}
          />
        </div>
      ) : null}

      {isOpenPopupProject ? (
        <CreateProject
          project={projectss}
          handlerCloseProject={handlerCloseProject}
          handerGetDataProject={handerGetDataProject}
          getDataCurrentIdProject={getDataCurrentIdProject}
        />
      ) : (
        <ListProjects getTime={getTime} projects={projectss} />
      )}
    </div>
  );
};

export default ProjectsContainers;

ProjectsContainers.defaultProps = {
  getTime: null,
};

ProjectsContainers.propTypes = {
  getTime: PropTypes.func,
};
