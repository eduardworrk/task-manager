import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { useHistory } from "react-router-dom";
import ProfileForm from "../../components/ProfileForm/ProfileForm";
import { saveProfile } from "../../module/action/action";

const ProfilePageContainer: React.FunctionComponent = () => {
  const profile = useSelector((state: any) => state.profile.profile);

  const dispatch = useDispatch();

  const editForm = (
    name: string,
    lastName: string,
    language: string,
    about: string
  ) => {
    dispatch(saveProfile({ name, lastName, language, about }));
  };
  const history = useHistory();
  const back = () => history.goBack();

  return (
    <div className="col-lg-8">
      <ProfileForm
        back={back}
        language={profile.language}
        editForm={editForm}
        name={profile.name}
        about={profile.about}
        lastName={profile.lastName}
      />
    </div>
  );
};

export default ProfilePageContainer;
