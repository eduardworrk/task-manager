import React, { useState } from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import { Redirect } from "react-router";
import { useSelector } from "react-redux";
import ProjectPage from "./pages/ProjectPage/ProjectPage";
import Header from "./core/organisms/Header/Header";
import ProjectsPage from "./pages/ProjectsPage/ProjectsPage";
import TaskPage from "./pages/TaskPage/TaskPage";
import Page404 from "./pages/404Page/Page404";
import StatisticsPage from "./pages/StatisticsPage/StatisticsPage";
import Footer from "./core/organisms/Footer/Footer";
import LoginPage from "./pages/LoginPage/LoginPage";
import ProfilePage from "./pages/ProfilePage/ProfilePage";

interface Project {
  main: any;
  name: string;
  id: number;
  tasks: any[];
}

const App: React.FC = () => {
  const [currentTaskName, setCurrentTask] = useState<string>("");
  const [currentTimeTask, setCurrentTimeTask] = useState<number>(0);
  const profileData = useSelector((state: any) => state.profile.profile);
  const [getGlobalSearch, setGetGlobalSearch] = useState<string>("");

  const projects = useSelector((state: Project) => state.main.projects);

  const globalSearch = (name: string) => {
    const project = [];
    let allTask = [];

    for (let i = 0; i < projects.length; i++) {
      project.push({
        title: projects[i].name,
        type: "project",
      });
      allTask.push(projects[i].tasks);
    }

    allTask = allTask.flat();
    const allData = [...allTask, ...project];

    return allData.filter((item: any) => {
      return `${item.title.toLowerCase()}`.indexOf(name) !== -1;
    });
  };

  // console.log(globalSearch("As"));

  const username = {
    name: profileData.name,
    lastName: profileData.lastName,
  };

  const getCurrentTaskName = (task: string) => {
    setCurrentTask(task);
  };

  const getNameGlobalSearch = (name: string) => {
    setGetGlobalSearch(name);
  };

  const getTime = (time: number) => {
    setCurrentTimeTask(time);
  };

  const [token, setToken] = useState<string>("");

  const getToken = (token: string) => {
    setToken(token);
  };

  if (!token) {
    return <LoginPage getToken={getToken} />;
  }

  return (
    <Router>
      <div className="wrapper">
        <Header
          profile={username}
          time={currentTimeTask}
          currentTaskName={currentTaskName}
          searchResult={globalSearch(getGlobalSearch)}
          getNameGlobalSearch={getNameGlobalSearch}
        />
        <div className="content">
          <div className="container">
            <div className="mt-4">
              <Switch>
                <Redirect exact from="/" to="/projects" />
                <Route exact path="/projects">
                  <ProjectsPage />
                </Route>

                <Route exact path="/projects/:id/create-task">
                  <TaskPage />
                </Route>

                <Route
                  path="/projects/:id/task/:id"
                  render={(option) => <TaskPage id={option} />}
                />

                <Route exact path="/projects/:id">
                  <ProjectPage
                    getTime={getTime}
                    currentTaskName={getCurrentTaskName}
                  />
                </Route>

                <Route
                  exact
                  path="/projects/:id/:slug"
                  render={(option) => (
                    <ProjectPage
                      id={option}
                      getTime={getTime}
                      currentTaskName={getCurrentTaskName}
                    />
                  )}
                />

                <Route exact path="/profile">
                  <ProfilePage />
                </Route>

                <Route exact path="/statistics">
                  <StatisticsPage />
                </Route>

                <Route>
                  <Page404 />
                </Route>
              </Switch>
            </div>
          </div>
        </div>
        <Footer />
      </div>
    </Router>
  );
};

export default App;
