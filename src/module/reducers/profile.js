import { SAVE_PROFILE } from "../types/types";
import dataTask from "../../services/data";

const initialState = {
  isLoading: false,
  profile: dataTask.profile,
};

const profileReducer = (state = initialState, action) => {
  switch (action.type) {
    case SAVE_PROFILE:
      return {
        ...state,
        profile: {
          ...state.profile,
          ...action.payload,
        },
      };

    default:
      return state;
  }
};

export default profileReducer;
