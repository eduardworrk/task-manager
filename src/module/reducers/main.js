import dataTask from "../../services/data";
import {
  ADD_ELEMENT,
  ADD_PROJECT,
  DELETE_PROJECT,
  DELETE_TASK,
  LOAD_TASK,
  OPEN_POPUP_PROJECT,
  PAUSE_TASK,
  PROJECT_CURRENT,
  PROJECT_REQUEST,
  RENAME_PROJECT,
  SAVE_TASK,
  SAVE_TIMER_TASK,
  START_TASK,
  STOP_TASK,
} from "../types/types";

const initialState = {
  isLoading: false,
  projects: dataTask.projects,
  startTask: false,
  editTask: false,
  currentTask: null,
};

const mainReducer = (state = initialState, action) => {
  const searchProject = state.projects.findIndex(
    (project) => project.id === state.currentProject
  );
  const currentProject = state.projects[searchProject];

  switch (action.type) {
    case PROJECT_REQUEST:
      return {
        isLoading: true,
        projects: action.payload,
        error: false,
        openCreateProjectPopup: false,
        currentProject: action.payload,
        startTask: false,
      };

    case OPEN_POPUP_PROJECT:
      return {
        ...state,
        openCreateProjectPopup: action.payload,
      };

    case PROJECT_CURRENT:
      return {
        ...state,
        currentProject: action.payload,
      };

    case ADD_PROJECT:
      return {
        ...state,
        projects: [...state.projects, action.payload],
      };

    case RENAME_PROJECT:
      return {
        ...state,
        projects: state.projects.map((item) => {
          if (item.id === currentProject.id) {
            return {
              ...item,
              name: action.payload,
            };
          }
          return {
            ...item,
          };
        }),
      };

    case DELETE_PROJECT:
      return {
        ...state,
        projects: state.projects.filter(
          (element) => element.id !== action.payload
        ),
      };

    case ADD_ELEMENT:
      currentProject.tasks = [...currentProject.tasks, action.payload];

      return {
        ...state,
        projects: [...state.projects],
      };

    case DELETE_TASK:
      return {
        ...state,
        projects: state.projects.map((item) => {
          if (item.id === currentProject.id) {
            return {
              ...item,
              tasks: item.tasks.filter(
                (element) => element.id !== action.payload
              ),
            };
          }

          return {
            ...item,
          };
        }),
      };

    case SAVE_TASK:
      return {
        ...state,
        editTask: false,
        projects: state.projects.map((item) => {
          if (item.id === currentProject.id) {
            return {
              ...item,
              tasks: item.tasks.map((element) => {
                if (element.id === action.payload.id) {
                  return {
                    ...element,
                    ...action.payload,
                  };
                }

                return {
                  ...element,
                };
              }),
            };
          }

          return {
            ...item,
          };
        }),
      };

    case LOAD_TASK:
      return {
        ...state,
        subTask: action.payload,
      };

    case SAVE_TIMER_TASK:
      return {
        ...state,
        startTask: false,
        editTask: true,
        projects: state.projects.map((item) => {
          if (item.id === currentProject.id) {
            return {
              ...item,
              tasks: item.tasks.map((element) => {
                if (element.id === action.id) {
                  return {
                    ...element,
                    time: action.payload,
                  };
                }

                return {
                  ...element,
                };
              }),
            };
          }

          return {
            ...item,
          };
        }),
        currentTask: null,
      };

    case START_TASK:
      return {
        ...state,
        startTask: true,
        currentTask: action.id,
      };

    case PAUSE_TASK:
      return {
        ...state,
        startTask: false,
      };

    default:
      return state;
  }
};

export default mainReducer;
