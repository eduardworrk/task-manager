import { combineReducers } from "redux";
import mainReducer from "./main";
import profileReducer from "./profile";

const rootReducer = combineReducers({
  main: mainReducer,
  profile: profileReducer,
});

export default rootReducer;
