import {
  ADD_ELEMENT,
  ADD_PROJECT,
  DELETE_PROJECT,
  DELETE_TASK,
  LOAD_TASK,
  OPEN_POPUP_PROJECT,
  PROJECT_CURRENT,
  PROJECT_REQUEST,
  RENAME_PROJECT,
  SAVE_TASK,
  SAVE_TIMER_TASK,
  START_TASK,
  PAUSE_TASK,
  SAVE_PROFILE,
} from "../types/types";

export const addElement = (element) => ({
  type: ADD_ELEMENT,
  payload: element,
});

export const deleteTask = (task) => ({
  type: DELETE_TASK,
  payload: task,
});

export const saveTask = (task, resetTime) => ({
  type: SAVE_TASK,
  payload: task,
  resetTime,
});

export const loadTask = (task) => ({
  type: LOAD_TASK,
  payload: task,
});

// export const startTask = (task, id, time) => ({
//   type: START_TASK,
//   payload: task,
//   id,
//   time,
// });

export const saveTimerTask = (timer, id) => ({
  type: SAVE_TIMER_TASK,
  payload: timer,
  id,
});

export const startTask = (task, id) => ({
  type: START_TASK,
  payload: task,
  id,
});

export const pauseTask = (task, id) => ({
  type: PAUSE_TASK,
  payload: task,
  id,
});

export const projectRequest = (task) => ({
  type: PROJECT_REQUEST,
  payload: task,
});

export const currentProject = (id) => ({
  type: PROJECT_CURRENT,
  payload: id,
});

export const openPopupProject = (boolean) => ({
  type: OPEN_POPUP_PROJECT,
  payload: boolean,
});

export const addProject = (project) => ({
  type: ADD_PROJECT,
  payload: project,
});

export const renameProject = (name) => ({
  type: RENAME_PROJECT,
  payload: name,
});

export const deleteProject = (id) => ({
  type: DELETE_PROJECT,
  payload: id,
});

export const saveProfile = (edit) => ({
  type: SAVE_PROFILE,
  payload: edit,
});
