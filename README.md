## Task manager (Frontend)

### install
```sh
$ npm i
```

### start
```sh
$ npm start
```



## Authors

- **Eduard Valiyllin** -  - [@eduardworrk](https://gitlab.com/eduardworrk)

## Main technologies

<img alt="React" src="https://img.shields.io/badge/react-%2320232a.svg?style=for-the-badge&logo=react&logoColor=%2361DAFB"/>

